#!/usr/bin/env bash
#SBATCH -n 1
#SBATCH --time=01:00:00
#SBATCH --mem=2500

MEMORY="2G"
CPUS="1"
MOUNTPATH="$SLURM_SUBMIT_DIR"
BASEIMAGE="${IMAGE:-../ubuntu-20.04-apptainer-500G.img}"

cd "$SLURM_SUBMIT_DIR/work" || exit 1
srun -- ./qemu-system-x86_64 -display none -hda "${BASEIMAGE}" -boot d -m "$MEMORY" -smp "$CPUS" -virtfs local,path="$MOUNTPATH",mount_tag=host0,security_model=none,id=host0
