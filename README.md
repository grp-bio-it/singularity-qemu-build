# About

This repository holds files and an Ubuntu 20.04 image used to build singularity containers

## TLDR

```
ssh username@login-gui.embl.de
mkdir /scratch/$USER/
cd /scratch/$USER/
module add git
module add git-lfs
git clone https://git.embl.de/grp-bio-it/singularity-qemu-build.git
cd singularity-qemu-build
git lfs install
git lfs pull
edit Singularity (use vi / nano / etc...)
./local-build-with-kvm.sh
```

On success your image will be `image.img`.

## How it works

In order to be able to build a singularity container, an operation that often requires access to a superuser account (i.e. `sudo`),
we use a fully virtualized machine powered by QEMU where you will run instructions in a Ubuntu 20.04 image as `root`.

This solution is designed to execute the script `build.sh` as root.
In turn the script calls `singularity` which reads the `Singularity` file present in this repository, ultimately creating a new container `image.img` in the repository folder.

To build your singularity container, modify instructions in the `Singularity` file.
If you need to install additional software in the VM or customize the process further, you can modify the `build.sh` file.

## Cloning this repository

You will need [`git-lfs`](https://git-lfs.github.com/) to obtain the larger files included in this repository.
If prompted for a username and password, leave both fields blank.

These files include `ubuntu-20.04-500G.img`, `work/qemu-system-x86_64` and several other files under `work/` that are required to execute the QEMU VM.

If your work folder is empty, make sure you have retrieved all Git LFS objects.

## Building a container

### On login-gui

The login-gui node in the EMBL cluster has KVM support enabled and is large enough for most needs.

To use this node, SSH to `login-gui.cluster.embl.de` or use the [graphical approach described in the cluster wiki](https://wiki.embl.de/cluster/Env#Graphical_Access).

You can then use the `local-build-with-kvm.sh` script to build your singularity container locally.

### SLURM

You can use one of two scripts to submit the job directly to SLURM:

    # This one will always execute but may be slow due to
    # complete virtualization emulation
    sbatch launch-build-job.sh

if virtualization support is enabled in the cluster you can instead:

    sbatch launch-build-job-with-kvm.sh

If the specified job requirements are not enough to build your image,
modify the `launch-build-job.sh` or `launch-build-job-with-kvm.sh` script.

## Launching QEMU

The current singularity QEMU image can build containers as large as ~500GB.
In order to build a singularity image, you must launch QEMU with the provided Ubuntu 20.04 image.
This will start a virtual machine that contains all the dependencies and runs `build.sh` script as root.

When launching QEMU, you can customize the virtual machine limits.
The most important setting is `MOUNTPATH` which will be used to share files with and from the virtual machine.
In addition and depening on the requirements of your singularity recipe, you can also adjust
the `MEMORY` and the total number of `CPUS`.

    MOUNTPATH=.
    MEMORY=2G
    CPUS=1

    ./qemu-system-x86_64 -display none -hda ubuntu-20.04-apptainer-500G.img -boot d -enable-kvm -m "$MEMORY" -smp "$CPUS" -virtfs local,path="$MOUNTPATH",mount_tag=host0,security_model=none,id=host0 &> /dev/null

By default the VM shuts down as soon as it finishes building or if the build scripts can't be found.
To prevent it for shutting down create an empty file `noshutdown` in `MOUNTPATH`.

See also the output of `qemu-system-x86_64 -help` for additional configuration options, in particular `-smp`.

You can also choose between the following images:

* `ubuntu-20.04-apptainer-500G.img` contains `apptainer` v1.0.0 (default choice)
* `ubuntu-20.04-singularity-500G.img` contains `singularity` v3.6.1

To use a different image execute `IMAGE=../path/to/file.img ./local-build-with-kvm.sh`.

# Prepare QEMU image

This step has already been done. The image exists in the repository as `ubuntu-20.04-500G.img`.

You need only these steps if you wish to use a different image or if the image provided is too old.

## Create an image that is big enough to be used to prepare large containers

    qemu-img create -f qcow2 ubuntu-20.04.img 500G

## Launch QEMU to install from CD

Download an ISO image (here Ubuntu 20.04 server) to boot from:

    qemu-system-x86_64 -hda ubuntu-20.04-500G.img -boot d -enable-kvm -m 2000 -virtfs local,path=.,mount_tag=host0,security_model=none,id=host0 -cdrom ubuntu-20.04-live-server-amd64.iso

You will need a KVM capable machine to run the above command. Currently `login-gui02` works.

During installation you will need to, unmount the root (LVM) volume, edit it, expand it to 500G (will be capped just below that value) and mount it as `/`.  
Create the user `appbuild` with password `appbuild` when asked for a user. Hostname could be `apptainer-build`.

QEMU will run a VNC interface. To connect to it from your computer use:

    ssh -L5900:localhost:5900 login-gui02.cluster.embl.de

and then on your computer install a vnc client such as TightVNC and run:

    vncviewer localhost::5900

## Launch image after installation

    qemu-system-x86_64 -hda ubuntu-20.04-500G.img -boot d -enable-kvm -m 2000 -virtfs local,path=.,mount_tag=host0,security_model=none,id=host0

You can login with `appbuild` as user and password and use `sudo -i` afterwards.

### Swap

We don't want to have swap in the VM so if the image contains a swap file in `/etc/fstab` remove it and the corresponding file on disk.

### Share folder with VM

In the machine:

    mkdir -p /share

and add the following to `/etc/fstab`:

    host0    /share    9p    trans=virtio,version=9p2000.L    0 0

after which:

    mount /share

### Install singularity and build environment

After mounting `/share` in the VM run:

    bash /share/build-singularity-ubuntu.sh

which will shutdown the VM when complete.
If the VM doesn't shutdown and a file named `noshutdown` doesn't exist in the mounted folder, something unexpected happened.


# Updating singularity

In order to update singularity, modify the `VERSION` in `build-singularity-ubuntu.sh`,
create the file `noshutdown` in the shared folder,
launch QEMU with the *Launch image after installation* instruction,
and run the `build-singularity-ubuntu.sh` script inside the VM.

## Shrinking the image

After an update, it may be necessary to [zerofill and convert](hhttps://stackoverflow.com/a/37337628) the VM disk to shrink the image.

Alternatively one can also use the [virtio-scsi](https://wiki.netbsd.org/tutorials/how_to_setup_virtio_scsi_with_qemu/)
interface to automatically shrink the disk.

# References

The static QEMU binaries bundled with this repository were compiled using: https://github.com/unode/qemu-static
