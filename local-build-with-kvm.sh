#!/usr/bin/env bash

WORKDIR="$(pwd)"
MEMORY="2G"
CPUS="1"
MOUNTPATH="$WORKDIR"
BASEIMAGE="${IMAGE:-../ubuntu-20.04-apptainer-500G.img}"

echo $BASEIMAGE
exit 1

cd "$WORKDIR/work" || exit 1
./qemu-system-x86_64 -display none -hda "${BASEIMAGE}" -boot d -enable-kvm -m "$MEMORY" -smp "$CPUS" -virtfs local,path="$MOUNTPATH",mount_tag=host0,security_model=none,id=host0
