#!/usr/bin/env bash

# This script was designed to be used with a fresh install of Ubuntu 20.04 server edition
# Login to the VM as root and run this script

export VERSION=1.0.0
export PATH=/usr/local/go/bin:${PATH}
export APPDEST=/opt/apptainer


# Remove snap packages and snapd which we don't need
if command -v snap > /dev/null ; then
    snap remove lxd
    snap remove core18
    snap remove snapd
    apt purge -y snapd
fi

apt update && apt install -y \
    squashfs-tools \
    wget \
    git \
    cryptsetup-bin || \
    { echo ">>> Failed to install dependencies <<<" ; exit 1; }

# Remove any old version of apptainer
rm -rf "${APPDEST}"

wget https://github.com/apptainer/apptainer/releases/download/v${VERSION}/apptainer_${VERSION}_amd64.deb && \
    apt install ./apptainer_${VERSION}_amd64.deb && \
    rm -f ./apptainer_${VERSION}_amd64.deb || \
    { echo ">>> Failed to build and install apptainer <<<" ; exit 1; }

# Add apptainer dir to PATH
SECURE_PATH_LINE="$(grep -n secure_path /etc/sudoers | cut -d: -f1)"
sed -i "${SECURE_PATH_LINE}s~/snap/~${APPDEST}/~" /etc/sudoers
grep -- "${APPDEST}" /etc/environment || sed -i "s~:/usr/local/games~:/usr/local/games:${APPDEST}/bin~" /etc/environment

# Remove build dependencies
apt purge -y \
    wget \
    git || \
    { echo ">>> Failed to remove post apptainer build dependencies <<<" ; exit 1; }

# And more cleanup
apt autoremove -y
apt purge -y ~c
apt clean

rm -rf /var/lib/apt/lists/*
rm -rf /var/log/journal/*

# The script that will build the image is triggered via systemd as soon
# as the system finishes to boot
cat << EOF > /root/start-build.sh
#!/usr/bin/env bash

source /etc/environment

if [ -f /share/build.sh ]; then
    echo "Executing 'build.sh' as root" >> /share/build.log
    cd /share
    if bash build.sh &>> build.log ; then
	echo "Successfully built the image" >> build.log
    else
	echo "Something went wrong while building the image. Check above for more information" >> build.log
    fi
else
    echo "Couldn't find 'build.sh' script in /share" >> /share/build.log
fi

if [ ! -f /share/noshutdown ]; then
    echo "Done with all work. Shutting down VM" >> /share/build.log
    systemctl poweroff
else
    echo "Keeping VM online. Manual action will be necessary to stop it." >> /share/build.log
fi
exit 0
EOF

chmod 755 /root/start-build.sh

# The following instructions configure systemd to run the above script
# as soon as boot is complete

cat << EOF > /etc/systemd/system/afterboot.target
[Unit]
Description=After boot Target
Requires=multi-user.target
After=multi-user.target
AllowIsolate=yes
EOF

mkdir -p /etc/systemd/system/afterboot.target.wants

cat << EOF > /etc/systemd/system/apptainer-build.service
[Unit]
Description=Launch apptainer build
After=multi-user.target

[Service]
Type=simple
ExecStart=/root/start-build.sh

[Install]
WantedBy=afterboot.target
EOF

ln -sf /etc/systemd/system/apptainer-build.service /etc/systemd/system/afterboot.target.wants/apptainer-build.service

systemctl daemon-reload

# Prepare image for shrinking
fstrim -va

# Boot our custom target by default
systemctl set-default afterboot.target

# This will cause the afterboot script to execute which will also shutdown the VM
systemctl isolate afterboot.target
